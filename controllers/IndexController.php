<?php
namespace app\controllers;
use yii;


class IndexController extends AppController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}