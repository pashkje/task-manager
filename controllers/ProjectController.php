<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 17.11.2016
 * Time: 17:36
 */

namespace app\controllers;

use yii;
use app\models\Project;

class ProjectController extends AppController
{
    public function actionIndex()
    {
        $filter = array();

        $by = Yii::$app->request->get('by');
        $order = Yii::$app->request->get('order');

        $base_by = ['ASC', 'DESC'];
        $base_order = ['name', 'login', 'url'];

        if (empty($by) || !in_array($by, $base_by)) $by = 'ASC';
        if (empty($order) || !in_array($order, $base_order)) $order = 'name';

        $projects = Project::GetList($order.' '.$by, $filter);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('index', ['items' => $projects]);
        } else {
            return $this->render('index', ['items' => $projects]);
        }

    }

    public function actionAdd()
    {
        $model = new Project();
        $request = Yii::$app->request;
        $form = $request->post('Project');

        if ($form) {
            $model->name = $form['name'];
            $model->url = $form['url'];
            $model->login = $form['login'];
            $model->password = $form['password'];
            $model->description = $form['description'];

            if ($model->validate() && $model->save()) {
                $this->redirect(['index']);
            }
        }
        return $this->render('add', ['model' => $model]);
    }

    public function actionEdit($id)
    {
        $project = Project::findOne($id);
        if (empty($project)) $this->redirect('/project');

        $request = Yii::$app->request;
        $form = $request->post('Project');

        if ($form) {

            $project->name = $form['name'];
            $project->url = $form['url'];
            $project->login = $form['login'];
            $project->password = $form['password'];
            $project->description = $form['description'];

            if ($project->validate() && $project->save()) {
                $this->redirect('/project/edit/'.$id);
            }
        }
        return $this->render('edit', ['item' => $project]);
    }

    public function actionPassword($id)
    {
        if (Yii::$app->request->isAjax) {
            $project = Project::findOne($id);
            return $project->password;
        }
    }



    public function actionDelete($id)
    {
        if (Yii::$app->request->isAjax) {
            $project = Project::findOne($id);
            $project->delete();
        }
    }


}