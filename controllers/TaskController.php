<?php
namespace app\controllers;
use yii;
use app\models\Tasks;
use app\models\TaskForm;
use app\models\Project;

class TaskController extends AppController {


    public function actionIndex()
    {
        $list = Tasks::getList();

        return $this->render('index', ['list' => $list]);
    }

    public function actionDetail($id)
    {
        $item = Tasks::findOne($id);
        if (!$item) return $this->redirect('/task',301);

        return $this->render('detail', ['item' => $item]);
    }

    public function actionAdd()
    {
        $project_list = Project::getNameList();

        array_unshift($project_list, 'Choose project');
        $model = new TaskForm();

        $request = Yii::$app->request;
        $form = $request->post('TaskForm');
        if (isset($form))
        {
            $model->title = $form['title'];
            $model->description = $form['description'];
            $model->project = $form['project'];

            if($model->validate() && $model->save()){
                $this->redirect(['index']);
            }
        }

        return $this->render('add', ['model' => $model, 'project_list' => $project_list]);
    }

    public function actionDelete($id)
    {
        if (Yii::$app->request->isAjax) {
            $task = Tasks::findOne($id);
            $task->delete();
        }
    }


}