<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 23.11.2016
 * Time: 17:51
 */

namespace app\models;

use yii\db\ActiveRecord;

class Project extends ActiveRecord
{

    public static function tableName()
    {
        return 'projects';
    }

    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
        ];
    }

    public static function getList($order = 'id DESC', $filter = array())
    {
        $result = self::find()->orderBy($order)->all();
        return $result;
    }

    public static function getNameList()
    {
        $query = self::find()->select('id, name')->orderBy('name ASC')->all();

        $result = [];

        foreach($query as $key=>$value)
        {
            $result[$value->id] = $value->name;
        }

        return $result;
    }
}