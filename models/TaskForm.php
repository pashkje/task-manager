<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 16.11.2016
 * Time: 20:42
 */

namespace app\models;

use Yii;

class TaskForm extends Tasks {

    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
        ];
    }

}