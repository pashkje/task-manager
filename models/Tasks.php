<?php

namespace app\models;
use yii\db\ActiveRecord;

class Tasks extends ActiveRecord {

    public static function tableName()
    {
        return 'tasks';
    }

    public static function getList()
    {
        $result = self::find()->orderBy('id DESC')->all();
        return $result;
    }
}