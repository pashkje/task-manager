<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?
$this->title = 'Create project';
$this->params['breadcrumbs'][] = ['label' => 'Project list', 'url' => ['/project']];
$this->params['breadcrumbs'][] = $this->title;

?>
    <h1><?= $this->title; ?></h1>
    <a href="/project" title="Go back" class="btn btn-success">
        <span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Go back
    </a>
    <br>
    <br>

<?
$form = ActiveForm::begin([
    'id' => 'project-form',
]) ?>

<?= $form->field($model, 'name'); ?>
<?= $form->field($model, 'url'); ?>
<?= $form->field($model, 'login'); ?>
<?= $form->field($model, 'password'); ?>
<?= $form->field($model, 'description')->textArea(['rows' => '6']); ?>

    <div class="form-group">
        <div class="">
            <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'title' => 'Create project']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>