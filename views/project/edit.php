<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?
$this->title = 'Edit project';
$this->params['breadcrumbs'][] = ['label' => 'Project list', 'url' => ['/project']];
$this->params['breadcrumbs'][] = $this->title;

?>
    <h1><?= $item->name; ?></h1>
    <a href="/project" title="Go back" class="btn btn-success">
        <span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Go back
    </a>
    <br>
    <br>

<?
$form = ActiveForm::begin([
    'id' => 'project-form',
]) ?>

<?= $form->field($item, 'name'); ?>
<?= $form->field($item, 'url'); ?>
<?= $form->field($item, 'login'); ?>
<?= $form->field($item, 'password'); ?>
<?= $form->field($item, 'description')->textArea(['rows' => '6']); ?>

    <div class="form-group">
        <div class="">
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'title' => 'Update project']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>