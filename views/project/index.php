<?
if(!Yii::$app->request->isAjax):
$this->title = 'Project list page';
$this->params['breadcrumbs'][] = 'Project list';

?>
<h1>Project list</h1>
<a href="/project/add" class="btn btn-primary" title="Add new project">Add project</a>
<br>
<br>
<?endif;?>
<? if (isset($items) && !empty($items)): ?>
    <?if(!Yii::$app->request->isAjax):?>
    <table class="table table-condensed table-project-list">
        <thead>
        <tr>
            <th data-sort="name" class="sortable">Name</th>
            <th data-sort="url" class="sortable">Url</th>
            <th data-sort="login" class="sortable">Login</th>
            <th style="text-align:center;">Password</th>
            <th>Edit</th>
            <th style="text-align:center;">Delete</th>
        </tr>
        </thead>
        <tbody>
    <?endif;?>
        <? foreach ($items as $value): ?>
            <? if (!isset($value->name)) break;?>
            <tr>
                <td><?= $value->name; ?></td>
                <td>
                    <a href="<?= (stristr($value->url, 'http') ? $value->url : 'http://'.$value->url); ?>" target="_blank" title="Open in new window">
                        <?
                        $value->url = str_replace('http://', '', $value->url);
                        $value->url = str_replace('https://', '', $value->url);
                        ?>
                        <?= $value->url; ?>
                    </a>
                </td>
                <td><?= $value->login; ?></td>
                <td align="center"><a title="View password" href="/project/get_password/<?= $value->id; ?>" class="password-mask-js">view <i
                            class="glyphicon glyphicon-eye-open"></i></a></td>
                <td><a href="/project/edit/<?= $value->id; ?>" title="Edit project">Edit</a></td>
                <td align="center">
                    <a href="/project/delete/<?= $value->id; ?>" class="remove-link-js" data-parent="tr"
                       data-sure="project">
                        <i class="glyphicon glyphicon-remove remove-project"></i>
                    </a>
                </td>
            </tr>
        <? endforeach; ?>
    <?if(!Yii::$app->request->isAjax):?>
        </tbody>
    </table>
    <?endif;?>
<? endif; ?>
