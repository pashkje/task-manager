<?php
use yii\helpers\Html;

use yii\widgets\ActiveForm;
use app\models\Project;

?>
<?
$this->title = 'Create task';
$this->params['breadcrumbs'][] = ['label' => 'Task list', 'url' => ['/task']];
$this->params['breadcrumbs'][] = $this->title;

?>
    <h1><?= $this->title; ?></h1>
    <a href="/task" title="Go back" class="btn btn-success">
        <span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Go back
    </a>
    <br>
    <br>

<?
$form = ActiveForm::begin([
    'id' => 'task-form',

]) ?>

<?= $form->field($model, 'title'); ?>
<?= $form->field($model, 'description')->textArea(['rows' => '6']); ?>
<?= $form->field($model, 'project')->dropDownList($project_list);?>



    <div class="form-group">
        <div class="">
            <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'title' => 'Create task']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>