<?
$this->title = $item->title;
$this->params['breadcrumbs'][] = ['label' => 'Task list', 'url' => ['/task']];
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?=$item->title;?></h1>
<a href="/task" title="Go back" class="btn btn-success">
    <span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Go back
</a>
<br>
<br>

<small><i><?=$item->date_created;?></i></small>
<br>
<br>
<div class="alert alert-info" style="white-space: pre-wrap;"><?=$item->description;?></div>