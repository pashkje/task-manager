<?
$this->title = 'Task list';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1>Task list</h1>
<a href="task/add" title="add new task" class="btn btn-primary">Add new task</a>
<br>
<br>
<?if(isset($list)):?>
    <?foreach($list as $item):?>
        <div class="alert alert-success task-item">
            <a href="/task/<?=$item->id;?>" title="Open detail"><?=$item->title;?></a>
            <i class="glyphicon glyphicon-remove pull-right remove-task remove-task-js" data-id="<?=$item->id;?>" title="Remove task"></i>
            <div class="span6 big-date pull-right">
                <span><?=date('d', strtotime($item->date_created));?></span>
                <br>
                <small><?=date('M', strtotime($item->date_created));?></small>
            </div>

        </div>
    <?endforeach;?>
<?else:?>
    <h1>Tasks not found</h1>
<?endif;?>
