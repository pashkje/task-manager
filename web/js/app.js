/**
 * Created by bitrix on 17.11.2016.
 */
$('.remove-task-js').on('click', function(){
    var _this_ = $(this);
    if (confirm('Are you sure?'))
    {
        $.ajax({
            url:'/task/delete/'+$(this).data('id'),
            success:function()
            {
                _this_.parents('.task-item').remove();
            }
        })
    }
})

$('.remove-link-js').on('click', function(){
    var element = $(this);

    if (confirm('Are you sure delete this '+element.data("sure")+'?'))
    {
        $.ajax({
            url:element.prop('href'),
            success:function()
            {
                element.parents(element.data('parent')).fadeOut(100);
            }
        })
    }
    return false;
})

$('.password-mask-js').on('click', function(){
    var item = $(this);
    var link = item.prop('href');
    $.ajax({
        url:link,
        success:function(response)
        {
            item.parent().html('<span>'+response+'</span>');
        }

    })
    return false;
})

var order_by_project = 'ASC';
$('.sortable').on('click', function(){
    var order_sort_project = $(this).data('sort');

    if (order_by_project == 'ASC') {
        order_by_project = 'DESC';
    } else {
        order_by_project = 'ASC';
    }

    var link = '?order='+order_sort_project+'&by='+order_by_project;

    $.ajax({
        url: link,
        headers: {
            'Cache-Control': 'max-age=0'
        },
        cache:false,
        success:function(data)
        {
            $('.table-project-list tbody').html(data);
            window.history.pushState(null, null, link);
        }

    })
})

    $(document).on('submit', '#FORM', function(){
        $.ajax({
            url:'/curl',
            type:'POST',
            data: $('form').serialize(),
            success:function(respose){
                $('textarea').html(respose);
            }
        })
        return false;
    })